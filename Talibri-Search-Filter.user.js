// ==UserScript==
// @name         Talibri: Search Filter
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Provide a search box to filter selections in market menus
// @author       Amraki
// @match        https://talibri.com/trade/1
// @grant        none
// ==/UserScript==


$(function() {
    'use strict';

    console.log("Search Filter loaded");

    // the IIFE lets us use a local variable store information
    var optionsCache = [];

    // add option values to the cache
    function optionsArray(select) {
        var reduce = Array.prototype.reduce;
        return reduce.call(select.options, function addToCache(options, option) {
            options.push(option);
            return options;
        }, []);
    }

    // give a list of options matching the filter value
    function filterOptions(filterValue, optionsCache) {
        return optionsCache.reduce(function filterCache(options, option) {
            var optionText = option.textContent;
            if (option.text.toLowerCase().match(filterValue.toLowerCase())) {
                options.push(option);
            }
            return options;
        }, []);
    }

    // replace the current options with the new options
    function replaceOptions(select, options) {
        while (select.options.length > 0) {
            select.remove(0);
        }
        options.forEach(function addOption(option) {
            select.add(option);
        });
    }

    // cache the options (if need be), and filter the options
    function filterOptionsHandler(evt) {
        var filterField = evt.target;
        var targetSelect = document.getElementById("order-item");
        if (optionsCache.length < 1) {
            optionsCache = optionsArray(targetSelect);
        }
        var options = filterOptions(filterField.value, optionsCache);
        replaceOptions(targetSelect, options);
    }

    function init() {
        setTimeout(function() {
            var maxAttempts = 5;
            var currAttempt = 1;
            var retVal = setInterval(() => {
                // stop conditions
                if (currAttempt >= maxAttempts || $('#search').length > 0) {
                    clearInterval(retVal);
                    return;
                }

                // add search box
                var label = $('form .form-group label:eq(0)');
                label.after("<br><input id='search' value='dropdown filter'>");

                // attach filter event to trigger on keyup
                var filter = document.getElementById("search");
                filter.addEventListener("keyup", filterOptionsHandler, false);

                currAttempt++;
            }, 500);
        }, 500);
    };

    $('button:contains("Post Purchase Order")').on("click", init);
    $('button:contains("Post Material Sale")').on("click", init);
});
